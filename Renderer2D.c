#include "Renderer2D.h"
#include "Texture.h"

struct render2d
{
   SDL_Texture * pawns;
};

static struct render2d Data;

void renderer2DLoad(SDL_Renderer * rend)
{
   Data.pawns = Texture_load(rend, "assets/pawns.png", NULL, NULL);
}

void renderer2DFree(void)
{
   SDL_DestroyTexture(Data.pawns);
}


void renderer2DRender(SDL_Renderer * rend, struct simulation * simulation)
{
   SDL_Rect src, dest;
   size_t i, count;
   struct pawn ** pawns;
   
   src.w = 32;
   src.h = 32;
   dest.w = 32;
   dest.h = 32;
   
   pawns = listGetArray_pawnPtr(&simulation->pawns, &count);
   for(i = 0; i < count; i ++)
   {
      if(pawns[i]->isPlayer)
      {
         src.x = 0;
         src.y = 0;
      }
      else
      {
         src.x = 32;
         src.y = 0;
      }

      dest.x = pawns[i]->x * 32;
      dest.y = pawns[i]->y * 32;
      
      SDL_RenderCopy(rend, Data.pawns, &src, &dest);
   }
}

