#include <stdlib.h>

#include "Simulation.h"

void timerManagerInit(struct timerManager * manager)
{
   listInit_moveTimerPtr(&manager->timerList, 0, 0);
   manager->ticksRemaining = 0;
}

void timerManagerTeardown(struct timerManager * manager)
{
   listFree_moveTimerPtr(&manager->timerList);   
}

void timerManagerAddTimer(struct timerManager * manager, struct moveTimer * timer)
{
   struct moveTimer ** ptrList;
   size_t count, i, insertIndex;
   ptrList = listGetArray_moveTimerPtr(&manager->timerList, &count);
   insertIndex = count;
   for(i = 0; i < count; i++)
   {
      if(ptrList[i]->ticks > timer->ticks)
      {
         insertIndex = i;
         break;
      }
   }
   listInsertValue_moveTimerPtr(&manager->timerList, insertIndex, timer);
}

void timerManagerRemoveTimer(struct timerManager * manager, struct moveTimer * timer)
{
   size_t index;
   struct moveTimer ** foundTimer = listFindValue_moveTimerPtr(&manager->timerList, 0, timer, &index);
   if(foundTimer != NULL)
   {
      listRemoveOrdered_moveTimerPtr(&manager->timerList, index);
   }
}


static inline
void timerManagerSortTimers(struct timerManager * manager)
{
   size_t i, k, count;
   struct moveTimer ** timers;
   
   timers = listGetArray_moveTimerPtr(&manager->timerList, &count);
   for(i = count - 2; i < count; i--)
   {
      for(k = i + 1; k < count; k++)
      {
         if(timers[k - 1]->ticks > timers[k]->ticks)
         {
            listSwap_moveTimerPtr(&manager->timerList, k - 1, k);
         }
      }
   }
}

static inline
void timerManagerPassTime(struct timerManager * manager, int ticks)
{
   size_t i, count;
   struct moveTimer ** timers;
   
   timers = listGetArray_moveTimerPtr(&manager->timerList, &count);
   for(i = 0; i < count; i++)
   {
      timers[i]->ticks -= ticks;
   }
   manager->ticksRemaining -= ticks;
}

bool timerManagerTick(struct timerManager * manager, int stopAtTicks)
{
   struct moveTimer * timer;
   
   if(listGetCount_moveTimerPtr(&manager->timerList) == 0)
   {
      manager->ticksRemaining = 0;
      return false;
   }
   
   timerManagerSortTimers(manager);
   
   timer = listGetValue_moveTimerPtr(&manager->timerList, 0, NULL);
   
   if(manager->ticksRemaining == 0)
   {
      manager->ticksRemaining = stopAtTicks;
   }
   
   if(manager->ticksRemaining < timer->ticks)
   {
      timerManagerPassTime(manager, manager->ticksRemaining);
      return false;
   }
   else 
   {
      timerManagerPassTime(manager, timer->ticks);
      return true;
   }
}


void simulationInit(struct simulation * simulation)
{
   timerManagerInit(&simulation->timerManager);
   listInit_pawnPtr(&simulation->pawns, 0, 0);
}

void simulationTeardown(struct simulation * simulation)
{
   size_t i, count;
   struct pawn ** pawns;
   timerManagerTeardown(&simulation->timerManager);
   
   pawns = listGetArray_pawnPtr(&simulation->pawns, &count);
   for(i = 0; i < count; i++)
   {
      free(pawns[i]);
   }
   
   listFree_pawnPtr(&simulation->pawns);
}

struct pawn * simulationCreatePawn(struct simulation * simulation)
{
   struct pawn * pawn = malloc(sizeof(struct pawn));
   pawn->x = 0;
   pawn->y = 0;
   pawn->moveTimer.ticks = 0;
   
   listAddValue_pawnPtr(&simulation->pawns, pawn, NULL);
   timerManagerAddTimer(&simulation->timerManager, &pawn->moveTimer);
   return pawn;
}



static inline
struct pawn * moveTimerGetPawnPtr(struct moveTimer * moveTimer)
{
   unsigned char * ptr = (unsigned char*)moveTimer;
   ptr -= offsetof(struct pawn, moveTimer);
   return (struct pawn *)ptr;
}


static inline
void simulationUpdatePawn(struct simulation * simulation, struct pawn * pawn)
{
   // TODO: AI movement
}

void simulationUpdate(struct simulation * simulation)
{
   if(timerManagerTick(&simulation->timerManager, 100))
   {
      size_t i, count;
      struct moveTimer ** timers;
      timers = listGetArray_moveTimerPtr(&simulation->timerManager.timerList, &count);
      for(i = 0; i < count; i++)
      {
         struct pawn * pawn;
         // The list should be sorted so all zeros are up front
         if(timers[i]->ticks > 0)
         {
            break;
         }
         pawn = moveTimerGetPawnPtr(timers[i]);

         if(!pawn->isPlayer)
         {
            simulationUpdatePawn(simulation, pawn);
         }
      }      
   }
}



