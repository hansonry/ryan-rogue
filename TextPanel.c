#include <stdlib.h>

#include "TextPanel.h"



struct TP_character
{
   unsigned char index;
   TextPanelFlags flags;
};

struct TextPanel
{
   int characterWidth;
   int characterHeight;
   int textureWidthInCharacters;
   TextPanelFlags flags;
   int cursorX;
   int cursorY;
   struct TP_character * buffer;
   size_t bufferSize;
   int widthInCharacters;
   int heightInCharacters;
};

static const SDL_Color TextPanelPalet[TEXTPANEL_COLOR_MASK + 1] = {
   { 0x00, 0x00, 0x00, SDL_ALPHA_OPAQUE },
   { 0x00, 0x00, 0x8B, SDL_ALPHA_OPAQUE },
   { 0x00, 0x64, 0x00, SDL_ALPHA_OPAQUE },
   { 0x00, 0x8B, 0x8B, SDL_ALPHA_OPAQUE },
   { 0x8B, 0x00, 0x00, SDL_ALPHA_OPAQUE },
   { 0x8B, 0x00, 0x8B, SDL_ALPHA_OPAQUE },
   { 0xDA, 0xA5, 0x20, SDL_ALPHA_OPAQUE },
   { 0xD3, 0xD3, 0xD3, SDL_ALPHA_OPAQUE },
   { 0x69, 0x69, 0x69, SDL_ALPHA_OPAQUE },
   { 0x00, 0x00, 0xFF, SDL_ALPHA_OPAQUE },
   { 0x15, 0xFF, 0x15, SDL_ALPHA_OPAQUE },
   { 0x15, 0xFF, 0xFF, SDL_ALPHA_OPAQUE },
   { 0xFF, 0x15, 0x15, SDL_ALPHA_OPAQUE },
   { 0xFF, 0x15, 0xFF, SDL_ALPHA_OPAQUE },
   { 0xFF, 0xFF, 0x00, SDL_ALPHA_OPAQUE },
   { 0xFF, 0xFF, 0xFF, SDL_ALPHA_OPAQUE }
};


struct TextPanel * TextPanel_new(int characterWidth, int characterHeight, 
                                 int textureWidthInCharacters, 
                                 int panelWidthInCharacters, int panelHeightInCharacters)
{
   struct TextPanel * panel = malloc(sizeof(struct TextPanel));
   panel->characterWidth           = characterWidth;
   panel->characterHeight          = characterHeight;
   panel->textureWidthInCharacters = textureWidthInCharacters;
   panel->widthInCharacters        = panelWidthInCharacters;
   panel->heightInCharacters       = panelHeightInCharacters;
   panel->bufferSize = panel->widthInCharacters * panel->heightInCharacters;
   panel->buffer = malloc(sizeof(struct TP_character) * panel->bufferSize);
   TextPanel_setFlags(panel, TEXTPANEL_FLAGS_FOREGROUND(TEXTPANEL_COLOR_WHITE, 0));
   TextPanel_setPosition(panel, 0, 0);
   TextPanel_clear(panel);
   return panel;
}

void TextPanel_destroy(struct TextPanel * panel)
{
   free(panel->buffer);
   free(panel);
}

void TextPanel_clear(struct TextPanel * panel)
{
   size_t i;
   for(i = 0; i < panel->bufferSize; i++)
   {
      panel->buffer[i].index = ' ';
      panel->buffer[i].flags = panel->flags;
   }
}

void TextPanel_setFlags(struct TextPanel * panel, TextPanelFlags flags)
{
   panel->flags = flags;
}

void TextPanel_setPosition(struct TextPanel * panel, int x, int y)
{
   panel->cursorX = x;
   panel->cursorY = y;
}

static inline
size_t TextPanel_getIndex(const struct TextPanel * panel, int x, int y)
{
   return x + y * panel->widthInCharacters;
}

void TextPanel_text(struct TextPanel * panel, const char * text)
{
   size_t index = TextPanel_getIndex(panel, panel->cursorX, panel->cursorY);
   while((*text) != '\0' && panel->cursorX < panel->widthInCharacters)
   {
      panel->buffer[index].index = *text;
      panel->buffer[index].flags = panel->flags;
      index ++;
      panel->cursorX ++;
      text ++;
   }
}

static inline
void TextPanel_setSourceRect(const struct TextPanel * panel, int index, SDL_Rect * src)
{
   int xPos = index % panel->textureWidthInCharacters;
   int yPos = index / panel->textureWidthInCharacters; 

   src->x = xPos * (panel->characterWidth  + 1) + 1;
   src->y = yPos * (panel->characterHeight + 1) + 1;
   src->w = panel->characterWidth;
   src->h = panel->characterHeight;
}


void TextPanel_render(struct TextPanel * panel, SDL_Renderer * rend, SDL_Texture * texture, int x, int y)
{
   SDL_Rect dest, src;
   int xCount = 0;
   size_t i;   

   dest.x = x;
   dest.y = y;
   dest.w = panel->characterWidth;
   dest.h = panel->characterHeight;
   
   for(i = 0; i < panel->bufferSize; i++)
   {
      const struct TP_character * data = &panel->buffer[i];
      int foregroundColor       = (data->flags & TEXTPANEL_FLAGS_FOREGROUND_COLOR_MASK)       >> TEXTPANEL_FLAGS_FOREGROUND_COLOR_SHIFT;
      int foregroundBlink       = (data->flags & TEXTPANEL_FLAGS_FOREGROUND_BLINK_FLAG)       == TEXTPANEL_FLAGS_FOREGROUND_BLINK_FLAG;
      int backgroundColor       = (data->flags & TEXTPANEL_FLAGS_BACKGROUND_COLOR_MASK)       >> TEXTPANEL_FLAGS_BACKGROUND_COLOR_SHIFT;
      int backgroundBlink       = (data->flags & TEXTPANEL_FLAGS_BACKGROUND_BLINK_FLAG)       == TEXTPANEL_FLAGS_FOREGROUND_BLINK_FLAG;
      int backgroundTransparent = (data->flags & TEXTPANEL_FLAGS_BACKGROUND_TRANSPARENT_FLAG) == TEXTPANEL_FLAGS_BACKGROUND_TRANSPARENT_FLAG;
      
      //printf("%c %d %d %d %d\n", data->index, foregroundColor, foregroundBlink, backgroundColor, backgroundBlink, backgroundTransparent);
      if(data->index != ' ' || !backgroundTransparent)
      {
         // Draw Background Color
         if(!backgroundTransparent)
         {
            const SDL_Color * color = &TextPanelPalet[backgroundColor];
            SDL_SetRenderDrawColor(rend, color->r, color->g, color->b, color->a);
            SDL_RenderFillRect(rend, &dest);
         }
         
         // Draw Text
         if(data->index != ' ')
         {
            const SDL_Color * color = &TextPanelPalet[foregroundColor];
            TextPanel_setSourceRect(panel, data->index, &src);
            SDL_SetTextureColorMod(texture, color->r, color->g, color->b);
            SDL_RenderCopy(rend, texture, &src, &dest);
         }
      }
      
      dest.x += panel->characterWidth;
      xCount ++;
      if(xCount >= panel->widthInCharacters)
      {
         xCount = 0;
         dest.x = x;
         dest.y += panel->characterHeight;
      }
   }
}
