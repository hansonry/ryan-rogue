#ifndef __TEXTURE_H__
#define __TEXTURE_H__
#include "SDL.h"

SDL_Texture * Texture_load(SDL_Renderer * rend, const char * filename, int * width, int * height);
SDL_Texture * Texture_loadBitFont(SDL_Renderer * rend, const char * filename, int * width, int * height);

#endif // __TEXTURE_H__
