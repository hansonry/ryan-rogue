/*
 * ryan-rouge.
 * Copyright (C) 2021  Ryan Hanson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
// System Includes
#include <stdio.h>
#include <stdbool.h>

// SDL Includes
#include "SDL.h"
#include "SDL_image.h"

// Project Includes
#include "RyanLogger.h"
#include "Texture.h"
#include "TextPanel.h"
#include "Simulation.h"
#include "Renderer2D.h"


int main(int argc, char* argv[])
{  
   SDL_Window * window; 
   SDL_Renderer * rend;
   SDL_Event event;
   
   SDL_Texture * textTexture;
   SDL_Rect r;
   struct TextPanel * panel;
   struct simulation simulation;
   struct pawn * player;
   
   Uint32 prevTicks;
   bool running;

   SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2
   IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF);

   // Create an application window with the following settings:
   window = SDL_CreateWindow(
      "ryan-rouge",                      // window title
      SDL_WINDOWPOS_UNDEFINED,           // initial x position
      SDL_WINDOWPOS_UNDEFINED,           // initial y position
      1024,                       // width, in pixels
      768,                      // height, in pixels
      SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
   
   // Check that the window was successfully created
   if (window == NULL) {
      // In the case that the window could not be made...
      printf("Could not create window: %s\n", SDL_GetError());
      return 1;
   }
   
   rend = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
   
   textTexture = Texture_loadBitFont(rend, "XGA_8x16.png", &r.w, &r.h);
   
   renderer2DLoad(rend);
   
   panel = TextPanel_new(8, 16, 16, 20, 20);

   TextPanel_text(panel, "Hello World!");
   TextPanel_setPosition(panel, 0, 1);
   TextPanel_setFlags(panel, TEXTPANEL_FLAGS_FOREGROUND(TEXTPANEL_COLOR_RED, 0));
   TextPanel_text(panel, "This is a test");
   
   simulationInit(&simulation);
   player = simulationCreatePawn(&simulation);
   player->isPlayer = true;
   
   running = true;
   prevTicks = SDL_GetTicks();
   while(running)
   {
      Uint32 ticks;
      while(SDL_PollEvent(&event))
      {
         if(event.type == SDL_QUIT)
         {
            running = false;
         }
         else if(event.type == SDL_WINDOWEVENT)
         {
            if(event.window.event == SDL_WINDOWEVENT_RESIZED)
            {
               //ScreenWidth  = event.window.data1;
               //ScreenHeight = event.window.data2;
            }
         }
         if(pawnIsActionCompleted(player))
         {
            int x = player->x;
            int y = player->y;
            bool action = false;
            if(event.type == SDL_KEYDOWN)
            {
               switch(event.key.keysym.sym)
               {
               case SDLK_KP_6:
                  x ++;
                  action = true;
                  break;
               case SDLK_KP_9:
                  x ++;
                  y --;
                  action = true;
                  break;
               case SDLK_KP_8:
                  y --;
                  action = true;
                  break;
               case SDLK_KP_7:
                  x --;
                  y --;
                  action = true;
                  break;
               case SDLK_KP_4:
                  x --;
                  action = true;
                  break;
               case SDLK_KP_1:
                  x --;
                  y ++;
                  action = true;
                  break;
               case SDLK_KP_2:
                  y ++;
                  action = true;
                  break;
               case SDLK_KP_3:
                  x ++;
                  y ++;
                  action = true;
                  break;
               case SDLK_KP_5:
                  action = true;
                  break;
               }
            }
            
            if(action)
            {
               player->x = x;
               player->y = y;
               player->moveTimer.ticks = 100;
            }
         }
      }

      ticks = SDL_GetTicks();
      //update((double)(ticks - prevTicks) / 1000.0);
      simulationUpdate(&simulation);
      prevTicks = ticks;

      //render(rend);
      SDL_SetRenderDrawColor(rend, 0x00, 0x3F, 0x00, SDL_ALPHA_OPAQUE);
      SDL_RenderClear(rend);
      
      TextPanel_render(panel, rend, textTexture, 0, 0);
      
      renderer2DRender(rend, &simulation);
      
      // Show to user
      SDL_RenderPresent(rend);
      
   }
   
   simulationTeardown(&simulation);
   
   TextPanel_destroy(panel);
   
   renderer2DFree();
   
   SDL_DestroyTexture(textTexture);
  
   // Close and destroy the window
   SDL_DestroyWindow(window);
   
   // Clean up
   IMG_Quit();
   SDL_Quit();
   rlmLogDebug("End of program");
   return 0;
}

