#ifndef __SIMULATION_H__
#define __SIMULATION_H__
#include <stdbool.h>
#include "List.h"

struct moveTimer
{
   int ticks;
};

LIST_CREATE_INLINE_FUNCTIONS(moveTimerPtr, struct moveTimer *)

struct pawn
{
   int x;
   int y;
   struct moveTimer moveTimer;
   bool isPlayer;
};

static inline
bool pawnIsActionCompleted(const struct pawn  * pawn)
{
   return pawn->moveTimer.ticks <= 0;
}

LIST_CREATE_INLINE_FUNCTIONS(pawnPtr, struct pawn *)

struct timerManager
{
   struct list_moveTimerPtr timerList;
   int ticksRemaining;
};

void timerManagerInit(struct timerManager * manager);

void timerManagerTeardown(struct timerManager * manager);

void timerManagerAddTimer(struct timerManager * manager, struct moveTimer * timer);
void timerManagerRemoveTimer(struct timerManager * manager, struct moveTimer * timer);

bool timerManagerTick(struct timerManager * manager, int stopAtTicks);

struct simulation
{
   struct list_pawnPtr pawns;
   struct timerManager timerManager;
};

void simulationInit(struct simulation * simulation);

void simulationTeardown(struct simulation * simulation);

struct pawn * simulationCreatePawn(struct simulation * simulation);

void simulationUpdate(struct simulation * simulation);

#endif // __SIMULATION_H__
