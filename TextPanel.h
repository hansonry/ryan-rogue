#ifndef __TEXTPANEL_H__
#define __TEXTPANEL_H__
#include <stdint.h>

#include "SDL.h"


// flags bits layout
// 0..3 Forground Color
// 4 Forground Blink Flag
// 5..8 Background Color
// 9 Background Blink Flag
// 10 Background Transparent Flag

#define TEXTPANEL_COLOR_BLACK        0 
#define TEXTPANEL_COLOR_BLUE         1
#define TEXTPANEL_COLOR_GREEN        2
#define TEXTPANEL_COLOR_CYAN         3
#define TEXTPANEL_COLOR_RED          4
#define TEXTPANEL_COLOR_PURPLE       5
#define TEXTPANEL_COLOR_ORANGE       6
#define TEXTPANEL_COLOR_LIGHTGREY    7
#define TEXTPANEL_COLOR_GREY         8
#define TEXTPANEL_COLOR_LIGHTBLUE    9
#define TEXTPANEL_COLOR_LIGHTGREEN   10
#define TEXTPANEL_COLOR_LIGHTCYAN    11
#define TEXTPANEL_COLOR_LIGHTRED     12
#define TEXTPANEL_COLOR_LIGHTPURPLE  13
#define TEXTPANEL_COLOR_LIGHTORANGE  14
#define TEXTPANEL_COLOR_WHITE        15
#define TEXTPANEL_COLOR_MASK         0xF

#define TEXTPANEL_FLAGS_FOREGROUND_COLOR_SHIFT       0
#define TEXTPANEL_FLAGS_FOREGROUND_COLOR_MASK        TEXTPANEL_COLOR_MASK << TEXTPANEL_FLAGS_FOREGROUND_COLOR_SHIFT
#define TEXTPANEL_FLAGS_FOREGROUND_BLINK_FLAG        0x0010
#define TEXTPANEL_FLAGS_BACKGROUND_COLOR_SHIFT       5
#define TEXTPANEL_FLAGS_BACKGROUND_COLOR_MASK        TEXTPANEL_COLOR_MASK << TEXTPANEL_FLAGS_BACKGROUND_COLOR_SHIFT
#define TEXTPANEL_FLAGS_BACKGROUND_BLINK_FLAG        0x0200
#define TEXTPANEL_FLAGS_BACKGROUND_TRANSPARENT_FLAG  0x0400


#define TEXTPANEL_FLAGS_FOREGROUND_COLOR(color)       (((color) & TEXTPANEL_COLOR_MASK) << TEXTPANEL_FLAGS_FOREGROUND_COLOR_SHIFT)
#define TEXTPANEL_FLAGS_FOREGROUND_COLOR_BLINK(color) (TEXTPANEL_FLAGS_FOREGROUND_BLINK_FLAG | TEXTPANEL_FLAGS_FOREGROUND_COLOR(color))
#define TEXTPANEL_FLAGS_BACKGROUND_COLOR(color)       (((color) & TEXTPANEL_COLOR_MASK) << TEXTPANEL_FLAGS_BACKGROUND_COLOR_SHIFT)
#define TEXTPANEL_FLAGS_BACKGROUND_COLOR_BLINK(color) (TEXTPANEL_FLAGS_BACKGROUND_BLINK_FLAG | TEXTPANEL_FLAGS_BACKGROUND_COLOR(color))
#define TEXTPANEL_FLAGS_BACKGROUND_TRANSPARENT        TEXTPANEL_FLAGS_BACKGROUND_TRANSPARENT_FLAG

#define TEXTPANEL_FLAGS(foregroundColor, fgBlinkFlag, backgroundColor, bgBlinkFlag, bgTransparent) \
   TEXTPANEL_FLAGS_FOREGROUND_COLOR(foregroundColor) | ((fgBlinkFlag)?TEXTPANEL_FLAGS_FOREGROUND_BLINK_FLAG:0) | \
   TEXTPANEL_FLAGS_BACKGROUND_COLOR(backgroundColor) | ((bgBlinkFlag)?TEXTPANEL_FLAGS_BACKGROUND_BLINK_FLAG:0) | \
   ((bgTransparent)?TEXTPANEL_FLAGS_BACKGROUND_TRANSPARENT_FLAG:0)
   
#define TEXTPANEL_FLAGS_FOREGROUND(foregroundColor, fgBlinkFlag) TEXTPANEL_FLAGS(foregroundColor, fgBlinkFlag, 0, 0, 1)

typedef uint16_t TextPanelFlags;

struct TextPanel;

struct TextPanel * TextPanel_new(int characterWidth, int characterHeight, 
                                 int textureWidthInCharacters, 
                                 int panelWidthInCharacters, int panelHeightInCharacters);
void TextPanel_destroy(struct TextPanel * panel);

void TextPanel_clear(struct TextPanel * panel);
void TextPanel_setFlags(struct TextPanel * panel, TextPanelFlags flags);
void TextPanel_setPosition(struct TextPanel * panel, int x, int y);
void TextPanel_text(struct TextPanel * panel, const char * text);

void TextPanel_render(struct TextPanel * panel, SDL_Renderer * rend, SDL_Texture * texture, int x, int y);


#endif // __TEXTPANEL_H__
