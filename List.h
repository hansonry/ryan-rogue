#ifndef __LIST_H__
#define __LIST_H__
#include <stddef.h>
#include <stdbool.h>

struct list
{
   size_t size;
   size_t count;
   size_t elementSize;
   size_t growBy;
   void * base;
};

void listInit(struct list * list, size_t elementSize, size_t growBy, size_t initSize);
void listFree(struct list * list);

void * listAdd(struct list * list, size_t * index);
void * listAddCopy(struct list * list, const void * element, size_t * index);
void * listAddMany(struct list * list, const void * elements, size_t elementCount, size_t * firstIndex);

void * listGet(const struct list * list, size_t index);
void * listGetArray(const struct list * list, size_t * count);

bool listRemoveFast(struct list * list, size_t index);
bool listRemoveOrdered(struct list * list, size_t index);

void * listInsert(struct list * list, size_t index);
void * listInsertCopy(struct list * list, size_t index, const void * element);

void listClear(struct list * list);
bool listSwap(struct list * list, size_t index1, size_t index2);

void * listFind(struct list * list, size_t startIndex, const void * element, size_t * foundIndex);

#define LIST_CREATE_INLINE_FUNCTIONS(name, type)                                                                                \
struct list_ ## name                                                                                                            \
{                                                                                                                               \
   struct list list;                                                                                                            \
};                                                                                                                              \
                                                                                                                                \
static inline                                                                                                                   \
void listInit_ ## name (struct list_ ## name * list, size_t growBy, size_t initSize)                                            \
{                                                                                                                               \
   listInit(&list->list, sizeof(type), growBy, initSize);                                                                       \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
void listFree_ ## name (struct list_ ## name * list)                                                                            \
{                                                                                                                               \
   listFree(&list->list);                                                                                                       \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
void listInitDefaults_ ## name (struct list_ ## name * list)                                                                    \
{                                                                                                                               \
   listInit(&list->list, sizeof(type), 0, 0);                                                                                   \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listAdd_ ## name (struct list_ ## name * list, size_t * index)                                                           \
{                                                                                                                               \
   return listAdd(&list->list, index);                                                                                          \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listAddCopy_ ## name (struct list_ ## name * list, type const * element, size_t * index)                                 \
{                                                                                                                               \
   return listAddCopy(&list->list, element, index);                                                                             \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listAddMany_ ## name (struct list_ ## name * list, type const * elements, size_t elementCount, size_t * firstIndex)      \
{                                                                                                                               \
   return listAddMany(&list->list, elements, elementCount, firstIndex);                                                         \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listGet_ ## name (const struct list_ ## name * list, size_t index)                                                       \
{                                                                                                                               \
   return listGet(&list->list, index);                                                                                          \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listGetArray_ ## name (const struct list_ ## name * list, size_t * count)                                                \
{                                                                                                                               \
   return listGetArray(&list->list, count);                                                                                     \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
bool listRemoveFast_ ## name (struct list_ ## name * list, size_t index)                                                        \
{                                                                                                                               \
   return listRemoveFast(&list->list, index);                                                                                     \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
bool listRemoveOrdered_ ## name (struct list_ ## name * list, size_t index)                                                     \
{                                                                                                                               \
   return listRemoveOrdered(&list->list, index);                                                                                     \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
size_t listGetCount_ ## name (const struct list_ ## name * list)                                                                \
{                                                                                                                               \
   return list->list.count;                                                                                                     \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listInsert_ ## name (struct list_ ## name * list, size_t index)                                                          \
{                                                                                                                               \
   return listInsert(&list->list, index);                                                                                       \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listInsertCopy_ ## name (struct list_ ## name * list, size_t index, type const * element)                                \
{                                                                                                                               \
   return listInsertCopy(&list->list, index, element);                                                                          \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listAddValue_ ## name (struct list_ ## name * list, type value, size_t * index)                                          \
{                                                                                                                               \
   return listAddCopy(&list->list, &value, index);                                                                              \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type listGetValue_ ## name (const struct list_ ## name * list, size_t index, type defaultValue)                                 \
{                                                                                                                               \
   type * value;                                                                                                                \
   value = listGet(&list->list, index);                                                                                         \
   if(value != NULL)                                                                                                            \
   {                                                                                                                            \
      return *value;                                                                                                            \
   }                                                                                                                            \
   return defaultValue;                                                                                                         \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listInsertValue_ ## name (struct list_ ## name * list, size_t index, type value)                                         \
{                                                                                                                               \
   return listInsertCopy(&list->list, index, &value);                                                                           \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
void listClear_ ## name (struct list_ ## name * list)                                                                           \
{                                                                                                                               \
   listClear(&list->list);                                                                                                      \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
bool listSwap_ ## name (struct list_ ## name * list, size_t index1, size_t index2)                                              \
{                                                                                                                               \
   listSwap(&list->list, index1, index2);                                                                                       \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listFind_ ## name (struct list_ ## name * list, size_t startIndex, type const * element, size_t * foundIndex)            \
{                                                                                                                               \
   return listFind(&list->list, startIndex, element, foundIndex);                                                               \
}                                                                                                                               \
                                                                                                                                \
static inline                                                                                                                   \
type * listFindValue_ ## name (struct list_ ## name * list, size_t startIndex, type element, size_t * foundIndex)               \
{                                                                                                                               \
   return listFind(&list->list, startIndex, &element, foundIndex);                                                              \
}


#endif // __LIST_H__

