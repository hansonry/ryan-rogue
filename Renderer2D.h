#ifndef __RENDERER2D_H__
#define __RENDERER2D_H__
#include "SDL.h"
#include "Simulation.h"

void renderer2DLoad(SDL_Renderer * rend);
void renderer2DFree(void);

void renderer2DRender(SDL_Renderer * rend, struct simulation * simulation);

#endif // __RENDERER2D_H__
