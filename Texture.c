#include "RyanLogger.h"

// SDL Includes
#include "SDL.h"
#include "SDL_image.h"

// Header Include
#include "Texture.h"

SDL_Texture * Texture_load(SDL_Renderer * rend, const char * filename, int * width, int * height)
{
   SDL_Surface * surf;
   SDL_Texture * texture;
   surf = IMG_Load(filename);
   if(surf == NULL)
   {
      rlmLogError("Faild to load file \"%s\"", filename);
      return NULL;
   }
   texture = SDL_CreateTextureFromSurface(rend, surf);
   if(width != NULL)
   {
      (*width)  = surf->w;
   }
   if(height != NULL)
   {
      (*height) = surf->h;
   }
   SDL_FreeSurface(surf);
   return texture;
}

SDL_Texture * Texture_loadBitFont(SDL_Renderer * rend, const char * filename, int * width, int * height)
{
   SDL_Surface * surf;
   SDL_Texture * texture;
   surf = IMG_Load(filename);
   if(surf == NULL)
   {
      rlmLogError("Faild to load file \"%s\"", filename);
      return NULL;
   }
   SDL_SetColorKey(surf, SDL_TRUE, 0);
   texture = SDL_CreateTextureFromSurface(rend, surf);
   if(width != NULL)
   {
      (*width)  = surf->w;
   }
   if(height != NULL)
   {
      (*height) = surf->h;
   }
   SDL_FreeSurface(surf);
   return texture;
}
